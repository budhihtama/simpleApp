import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Image,
  Text,
  Modal,
  View,
  Button,
  TouchableOpacity,
  Linking,
} from "react-native";
import { Rating } from "react-native-elements";

export default function App() {
  const [resRating, setResRating] = useState();
  const [modalVisible, setModalVisible] = useState(false);

  const list = [
    {
      title: "Cancel",
    },
    {
      title: "Submit",
    },
  ];

  const cancel = () => {
    setModalVisible(false);
  };

  const submit = () => {
    if (resRating > 3) {
      Linking.openURL(
        "https://play.google.com/store/apps/details?id=com.m1.mym1&hl=en&gl=US"
      );
    }
    alert("Thanks for rating");
    setModalVisible(false);
  };

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Button title="Rate this app" onPress={() => setModalVisible(true)} />
      <Modal animationType="fade" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Image
              style={{
                width: 70,
                height: 70,
                marginVertical: 10,
                alignSelf: "center",
              }}
              source={require("./assets/favicon.png")}
            />
            <Text style={styles.modalText}>Enjoying Emoji Blitz?</Text>
            <Text style={styles.modalText2}>
              Tap a star to rate it on the App Store.
            </Text>

            <Rating
              imageSize={30}
              count={5}
              defaultRating={3}
              onFinishRating={(rating) => setResRating(rating)}
              tintColor={"rgba(255, 255, 255, 1)"}
              ratingColor={"#007aff"}
              type="custom"
              ratingBackgroundColor={"white"}
              style={{
                borderWidth: 0.5,
                marginVertical: 10,
                paddingVertical: 10,
              }}
            />

            <View
              style={{ flexDirection: "row", justifyContent: "space-around" }}
            >
              {list.map((i, index) => (
                <TouchableOpacity
                  onPress={() => {
                    index == 0 ? cancel() : submit();
                  }}
                  key={index}
                >
                  <Text style={styles.textStyle}>{i.title}</Text>
                </TouchableOpacity>
              ))}
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#222",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    padding: 10,
    backgroundColor: "blue",
    borderRadius: 10,
  },
  txt: {
    color: "white",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    width: "70%",
    margin: 20,
    backgroundColor: "rgba(255, 255, 255, 1)",
    borderRadius: 20,
    paddingVertical: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  textStyle: {
    color: "#007aff",
    textAlign: "center",
    fontSize: 20,
  },
  modalText: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 20,
  },
  modalText2: {
    marginBottom: 10,
    textAlign: "center",
    fontSize: 19,
    paddingHorizontal: 35,
  },
  cancel: {
    justifyContent: "center",
    marginTop: 35,
  },
});
