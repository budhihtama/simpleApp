# simpleApp
Project Exercise Test for make a Rate this app

Please read this readme file first before you start!

How to run the project
You need to install required dependencies (libraries) by typing in the terminal

yarn install
Then you can run this project by:

Using node
yarn start
Using Android
yarn android
